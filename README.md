# Ejercicios para practicar las bases de Python.

La tarea está compuesta de 3 ejercicios, descritos en el archivo "Tareas_Clase_2.pdf".  

### INSTRUCCIONES:

- Para responder a cada punto cree un notebook diferente: ejercicio1.ipynb, 
ejercicio2.ipynb, ejercicio3.ipynb
- Mantenga esos 3 archivos dentro de esta misma carpeta
- Cada notebook debe ser mucho más que un montón de líneas de comando, debe
contener explicaciones
- Inicie con un encabezado **identificándose** y describiendo el problema a
resolver
- Fraccione el código en celdas de acuerdo a la lógica de la solución
- Explique su estrategia de solución y la funcionalidad de las distintas 
partes del código, utilizando comentarios e intercalando celdas de markdown
- Muestre ejemplos desarrollados y comentandos
- Trabaje sus notebooks libremente, y cuando ya estén listos para ser
presentados, expórtelos a formato html, revisando que estén apareciendo los
resultados producidos por los ejemplos.
- Exploraciones complementarias al ejercicio serán muy bien recibidas

**El objetivo es que si su instructor desea correr el código pueda hacerlo,
para eso va el markdown, pero que esto no sea necesario para evaluar la tarea,
para eso va el html con todas las explicaciones y los ejemplos con resultados**





